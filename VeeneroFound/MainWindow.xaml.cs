﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VeeneroFound
{
    using System.Windows;
    using MahApps.Metro.Controls;
    using VeeneroFound.ViewModel;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();

            this.Bootstrapper = new AppBootstrapper();
            this.DataContext = this.Bootstrapper;

            this.Title = "Veenero Library";
        }

        /// <summary>
        /// Gets or sets bootstrap
        /// </summary>
        public AppBootstrapper Bootstrapper { get; protected set; }
    }
}
