﻿// <copyright file="AppBootstrapper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VeeneroFound.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ReactiveUI;
    using Splat;
    using UI.ProcessComponents.MainMenu;
    using UI.ProcessComponents.Users;
    using UI.VisualComponents.MainMenu;
    using UI.VisualComponents.Users;
    using VeeneroFound.Environment.Configuration;

    /// <summary>
    /// Routing in the application
    /// </summary>
    public class AppBootstrapper : ReactiveObject, IScreen
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppBootstrapper"/> class.
        /// </summary>
        /// <param name="dependencyResolver">dependency resolver</param>
        /// <param name="routingState">storing routes</param>
        public AppBootstrapper(IMutableDependencyResolver dependencyResolver = null, RoutingState routingState = null)
        {
            this.Router = routingState ?? new RoutingState();

            dependencyResolver = dependencyResolver ?? Locator.CurrentMutable;

            this.RegisterParts(dependencyResolver);

            LogHost.Default.Level = LogLevel.Debug;

            this.Router.Navigate.Execute(new MainMenuViewModel(this));

            AutoMapperConfiguration.Configure();
        }

        /// <summary>
        /// Gets variable for store live router ViewModel
        /// </summary>
        public RoutingState Router { get; }

        /// <summary>
        /// Registration of parts in a app
        /// </summary>
        /// <param name="dependencyResolver">dependency resolver for setting view for viewModel</param>
        private void RegisterParts(IMutableDependencyResolver dependencyResolver)
        {
            dependencyResolver.RegisterConstant(this, typeof(IScreen));

            dependencyResolver.Register(() => new MainMenuView(), typeof(IViewFor<MainMenuViewModel>));
            dependencyResolver.Register(() => new ControlMenuOfUsersView(), typeof(IViewFor<ControlMenuOfUsersViewModel>));
            dependencyResolver.Register(() => new EditableMenuOfUserView(), typeof(IViewFor<EditableMenuOfUserViewModel>));
            dependencyResolver.Register(() => new ReasonDeleteUserView(), typeof(IViewFor<ReasonDeleteUserViewModel>));
        }
    }
}
