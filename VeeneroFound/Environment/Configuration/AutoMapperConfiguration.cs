﻿// <copyright file="AutoMapperConfiguration.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace VeeneroFound.Environment.Configuration
{
    using AutoMapper;
    using UI.Infrastructure.Mappings;
    using VeeneroBuisness.Workflows.Mapping;

   /// <summary>
   /// AutoMapper Configuration
   /// </summary>
   public class AutoMapperConfiguration
   {
       /// <summary>
       /// Configure method
       /// </summary>
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<UserMapping>();
                x.AddProfile<UserProfile>();
            });

            Mapper.Configuration.AssertConfigurationIsValid();
        }
   }
}
