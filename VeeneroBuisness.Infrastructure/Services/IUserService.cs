﻿namespace VeeneroBuisness.Infrastructure.Services
{
    using System.Collections.Generic;

    using UI.Entities.Users;

    using VeeneroBuisness.Entities.User;

    public interface IUserService : IService
    {
        IList<UserDto> GetUser();

        UserDto GetUserByDto(UserDto userDto);

        void UpdateUser(UserDto userDto);

        void AddNewUser(UserDto userDto);
    }
}
