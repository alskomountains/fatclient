﻿namespace VeeneroBuisness.Infrastructure.Services
{
    using System.Collections.Generic;
    using VeeneroBuisness.Entities.User;

    public interface IUserTypeService : IService
   {
       IList<UserTypeDto> UserTypeDtos();
   }
}
