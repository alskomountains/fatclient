﻿using System.Collections.Generic;

namespace DataAccessLayer.Infrastructure
{
   public interface IRepository <T> where T : class
   {
       IList<T> GetList();

       T GetById(int id);

       void Create(T item);

       void Update(T item);
   }
}
