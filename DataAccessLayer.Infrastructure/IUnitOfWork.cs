﻿namespace DataAccessLayer.Infrastructure
{
    using System;
    using VeeneroLibraryModel.User;

    public interface IUnitOfWork : IDisposable
    {
        IRepository<User> UserRepository { get; }

        IRepository<UserType> UserTypeRepository { get; }
    }
}
