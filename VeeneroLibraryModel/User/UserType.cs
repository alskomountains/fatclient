﻿namespace VeeneroLibraryModel.User
{
    using System;

    /// <summary>
    /// Class of UserType
    /// </summary>
    public class UserType : IHasUid
    {
        /// <summary>
        /// Gets or sets id of Type
        /// </summary>
        public Guid id { get; set; }

        /// <summary>
        /// Gets or sets Type
        /// </summary>
        public string Type { get; set; }
    }
}
