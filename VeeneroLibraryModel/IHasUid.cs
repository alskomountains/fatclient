﻿namespace VeeneroLibraryModel
{
    using System;

    public interface IHasUid
    {
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        Guid id { get; set; }
    }
}
