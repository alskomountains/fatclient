﻿using AutoMapper;
using DataAccessLayer.Components.Repositories;
using VeeneroLibraryModel.User;

namespace VeeneroBuisness.Workflows.Services
{
    using System.Collections.Generic;
    using System.Linq;
    
    using DataAccessLayer.Infrastructure;
    using VeeneroBuisness.Entities.User;
    using VeeneroBuisness.Infrastructure.Services;

    public class UserService : IUserService
    {
        private  IUnitOfWork Database { get; set; }
        public UserService()
        {
            Database = new UnitOfWork();
        }

        public IList<UserDto> GetUser()
        {
           var repository = Database.UserRepository.GetList();
           
           return Mapper.Map<IList<UserDto>>(repository);
        }

        public void AddNewUser(UserDto userDto)
        {
            Database.UserRepository.Create(Mapper.Map<User>(userDto));
        }

        public void UpdateUser(UserDto userDto)
        {
            Database.UserRepository.Update(Mapper.Map<User>(userDto));
        }

        public UserDto GetUserByDto(UserDto userDto)
        {
            throw new System.NotImplementedException();
        }

        public void AppplyingChanges(UserDto userDto, long changesState)
        {
            switch (changesState)
            {
                case 1:
                    AddNewUser(userDto);
                    break;
                case 2:
                    UpdateUser(userDto);
                    break;
            }
        }
    }
}
