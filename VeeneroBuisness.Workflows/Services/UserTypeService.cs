﻿

namespace VeeneroBuisness.Workflows.Services
{
    using System;
    using System.Collections.Generic;

    using AutoMapper;
    using DataAccessLayer.Components.Repositories;
    using DataAccessLayer.Infrastructure;
    using VeeneroBuisness.Infrastructure.Services;
    using Entities.User;
    

    public class UserTypeService : IUserTypeService
    {
        private IUnitOfWork unitOfWork { get; set; }

        public UserTypeService()
        {
           unitOfWork = new UnitOfWork();
        }

        public IList<UserTypeDto> UserTypeDtos()
        {
            var repository = unitOfWork.UserTypeRepository.GetList();

            return Mapper.Map<IList<UserTypeDto>>(repository);
        }
    }
}
