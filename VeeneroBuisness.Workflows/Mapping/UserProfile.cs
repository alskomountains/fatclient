namespace VeeneroBuisness.Workflows.Mapping
{
    using AutoMapper;
    using Entities.User;
    using VeeneroLibraryModel.User;
    
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            this.CreateMap<User, UserDto>()
                .ForMember(x => x.Id, y => y.MapFrom(x => x.id))
                .ForMember(x => x.Name, y => y.MapFrom(x => x.Name))
                .ForMember(x => x.Description, y => y.MapFrom(x => x.Description))
                .ForMember(x => x.DeletedUser, y => y.MapFrom(x => x.DeletedUser))
                .ForMember(x => x.ReasonOfDeleted, y => y.MapFrom(x => x.ReasonOfDeleted))
                .ForMember(x => x.Gender, y => y.MapFrom(x => x.Gender))
                .ForMember(x => x.TypeOfUser, y => y.MapFrom(x => x.TypeOfUser));
            this.CreateMap<UserDto, User>()
                .ForMember(x => x.id, y => y.MapFrom(x => x.Id))
                .ForMember(x => x.Name, y => y.MapFrom(x => x.Name))
                .ForMember(x => x.Description, y => y.MapFrom(x => x.Description))
                .ForMember(x => x.DeletedUser, y => y.MapFrom(x => x.DeletedUser))
                .ForMember(x => x.ReasonOfDeleted, y => y.MapFrom(x => x.ReasonOfDeleted))
                .ForMember(x => x.Gender, y => y.MapFrom(x => x.Gender))
                .ForMember(x => x.TypeOfUser, y => y.MapFrom(x => x.TypeOfUser));
            this.CreateMap<UserType, UserTypeDto>()
                .ForMember(x => x.Id, y => y.MapFrom(x => x.id))
                .ForMember(x => x.Type, y => y.MapFrom(x => x.Type));
            this.CreateMap<UserTypeDto, UserType>()
                .ForMember(x => x.id, y => y.MapFrom(x => x.Id))
                .ForMember(x => x.Type, y => y.MapFrom(x => x.Type));
        }
    }
}