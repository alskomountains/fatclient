﻿// <copyright file="IHasUId.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.Entities
{
    using System;

    /// <summary>
    /// interface for installing Id in model
    /// </summary>
    public interface IHasUId
   {
       /// <summary>
       /// Gets or sets Id
       /// </summary>
        Guid Id { get; set; }
   }
}
