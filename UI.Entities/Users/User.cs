﻿// <copyright file="User.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.Entities.Users
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Class of User
    /// </summary>
    public class User : IHasUId
    {
        /// <summary>
        /// Gets or sets Id of User
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets Name of User
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets gender of user
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets Description of User
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets deleted status to user or not
        /// </summary>
        public bool? DeletedUser { get; set; }

        /// <summary>
        /// Gets or sets reason from deleting
        /// </summary>
        public string ReasonOfDeleted { get; set; }

        /// <summary>
        /// Gets or sets type of user
        /// </summary>
        public string TypeOfUser { get; set; }
    }
}
