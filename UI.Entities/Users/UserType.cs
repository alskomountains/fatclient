﻿// <copyright file="UserType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.Entities.Users
{
    using System;

    /// <summary>
    /// Class of UserType
    /// </summary>
    public class UserType : IHasUId
    {
        /// <summary>
        /// Gets or sets id 
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets Type
        /// </summary>
        public string Type { get; set; }
    }
}
