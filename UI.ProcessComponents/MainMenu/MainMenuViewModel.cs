﻿// <copyright file="MainMenuViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.ProcessComponents.MainMenu
{
    using System;
    using System.Reactive;
    using System.Reactive.Linq;
    using System.Windows.Input;
     
    using ReactiveUI;
    using UI.Infrastructure.ViewModel.MainMenu;
    using UI.ProcessComponents.Users;

    /// <summary>
    /// ViewModel for Main Menu View
    /// </summary>
    public class MainMenuViewModel : ReactiveObject, IMainMenuViewModel
    {   
        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuViewModel"/> class.
        /// </summary>
        /// <param name="screen">Screen for switch with ViewModels</param>
        public MainMenuViewModel(IScreen screen)
        {
            this.HostScreen = screen;
            this.RouteToControlListOfUsers = ReactiveCommand.CreateFromTask(async () =>
                await this.HostScreen.Router.Navigate.Execute(new ControlMenuOfUsersViewModel(this.HostScreen))
                    .Select(_ => Unit.Default));
        }

        /// <summary>
        /// Gets return UrlSegment for Screen
        /// </summary>
        public string UrlPathSegment
        {
            get
            {
                return "MainMenu";
            }
        }

        /// <summary>
        /// Gets Screen
        /// </summary>
        public IScreen HostScreen
        {
            get;
        }

        /// <summary>
        /// Gets command RouteToControlListOfUsers
        /// </summary>
        public ReactiveCommand<Unit, Unit> RouteToControlListOfUsers { get; }

        /// <summary>
        /// Gets back
        /// </summary>
        public ReactiveCommand<Unit, Unit> Back { get; }
    }
}
