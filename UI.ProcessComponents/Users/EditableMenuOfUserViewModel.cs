﻿// <copyright file="EditableMenuOfUserViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.ProcessComponents.Users
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Reactive;
    using System.Reactive.Linq;
    using System.Windows.Input;

    using AutoMapper;
    using DynamicData.Binding;
    using MahApps.Metro.Controls.Dialogs;
    using ReactiveUI;
    using UI.Entities.Users;
    using UI.Infrastructure.ViewModel.Users;
    using VeeneroBuisness.Entities.User;
    using VeeneroBuisness.Workflows.Services;

    /// <inheritdoc cref="EditableMenuOfUserViewModel"/>
    /// <summary>
    /// EditableMenu of User
    /// </summary>
    public class EditableMenuOfUserViewModel : ReactiveObject, IEditableMenuOfUserViewModel
    {
        /// <summary>
        /// service of users
        /// </summary>
        private readonly UserService userService;

        /// <summary>
        /// service of user types
        /// </summary>
        private readonly UserTypeService userTypeService;

        /// <summary>
        /// Command for creating new user
        /// </summary>
        private ReactiveCommand<Unit, Unit> applyCommand;

        /// <summary>
        /// dialog Coordinator for controls dialogs
        /// </summary>
        private IDialogCoordinator dialogCoordinator;

        /// <summary >
        /// Initializes a new instance of the <see cref="EditableMenuOfUserViewModel"/> class.
        /// </summary>
        /// <param name="screen"> for routing</param>
        /// <param name="user">for model</param>
        /// <param name="changesState"> for state of changes (new or edit)</param>
        /// <param name="coordinator"> for set coordinator</param>
        public EditableMenuOfUserViewModel(IScreen screen, User user, long changesState, IDialogCoordinator coordinator)
        {
            this.HostScreen = screen;
            this.Back = ReactiveCommand.CreateFromTask(async () =>
                await this.HostScreen.Router.NavigateAndReset
                    .Execute(new ControlMenuOfUsersViewModel(this.HostScreen))
                    .Select(_ => Unit.Default));

            this.userService = new UserService();
            this.userTypeService = new UserTypeService();

            this.User = user;
            this.UserTypes = new List<UserType>();

            this.ChangesState = changesState;
            this.NameOfAct = this.SetNameOfAct(changesState);

            this.dialogCoordinator = coordinator;

            this.GetUserTypes();
        }

        /// <summary>
        /// Gets UrlName for ViewModel
        /// </summary>
        public string UrlPathSegment
        {
            get
            {
                return "Create or Edit exists User";
            }
        }

        /// <summary>
        /// Gets Screen for Routing
        /// </summary>
        public IScreen HostScreen { get; }

        /// <summary>
        /// Gets Back
        /// </summary>
        public ReactiveCommand<Unit, Unit> Back { get; }

        /// <summary>
        /// Gets or sets user
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Gets or sets type of user
        /// </summary>
        public ObservableCollection<string> TypeOfUser { get; set; }

        /// <summary>
        /// Gets or sets list of user types
        /// </summary>
        public IList<UserType> UserTypes { get; set; }

        /// <summary>
        /// Gets command for apply changes
        /// </summary>
        public ICommand ApplyCommand
        {
            get
            {
                var canExecute = this.WhenAnyValue(x => x.User.Name, (name) => !string.IsNullOrEmpty(name));
                if (this.applyCommand == null)
                {
                    return this.applyCommand = this.ApplyCommand1(canExecute);
                }

                return this.applyCommand;
            }
        }

        /// <summary>
        /// Gets or sets State of pending changes
        /// </summary>
        public long ChangesState { get; set; }

        /// <summary>
        /// Gets or sets name of act
        /// </summary>
        public string NameOfAct { get; set; }

        /// <summary>
        /// reactive command creating new user
        /// </summary>
        /// <param name="canExecute">can execute</param>
        /// <returns>Returns reactive command creating new user</returns>
        private ReactiveCommand<Unit, Unit> ApplyCommand1(IObservable<bool> canExecute)
        {
            return ReactiveCommand.Create(() =>
            {
                if (!string.IsNullOrEmpty(this.User.Name))
                {
                    this.userService.AppplyingChanges(Mapper.Map<UserDto>(this.User), this.ChangesState);

                    this.HostScreen.Router.NavigateAndReset.Execute(new ControlMenuOfUsersViewModel(this.HostScreen));
                }
                else
                {
                    this.dialogCoordinator.ShowMessageAsync(this, "Warning", "You cannot leave user name is empty!");
                }
            });
        }

        /// <summary>
        /// sets name of act
        /// </summary>
        /// <param name="changes">get state for set name</param>
        /// <returns>Return name of act</returns>
        private string SetNameOfAct(long changes)
        {
            string name = string.Empty;

            switch (changes)
            {
                case 1:
                    name = "Create new user";
                    break;
                case 2:
                    name = "Editing user";
                    break;
            }

            return name;
        }

        /// <summary>
        /// get user types
        /// </summary>
        private void GetUserTypes()
        {
            var result = this.userTypeService.UserTypeDtos();
            this.UserTypes = Mapper.Map<IList<UserType>>(result);
        }
    }
}
