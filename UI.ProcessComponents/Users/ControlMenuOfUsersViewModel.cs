﻿// <copyright file="ControlMenuOfUsersViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.ProcessComponents.Users
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Reactive;
    using System.Reactive.Linq;
    
    using AutoMapper;
    using MahApps.Metro.Controls.Dialogs;
    using ReactiveUI;
    using UI.Entities.Users;
    using UI.Infrastructure.ViewModel.Users;
    using UI.ProcessComponents.MainMenu;
    using VeeneroBuisness.Workflows.Services;

    /// <summary>
    /// ViewModel For ControlMenuOfUsersView
    /// </summary>
    public class ControlMenuOfUsersViewModel : ReactiveObject, IControlMenuOfUsersViewModel
    {
        /// <summary>
        /// field of user service
        /// </summary>
        private readonly UserService userService;

        /// <summary>
        /// mapper for mapping :)
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlMenuOfUsersViewModel"/> class.
        /// </summary>
        /// <param name="screen">Screen for switch with ViewModels</param>
        public ControlMenuOfUsersViewModel(IScreen screen)
        {
            this.HostScreen = screen;
            this.Back = this.HostScreen.Router.NavigateBack;
            this.AddUserCommand = ReactiveCommand.CreateFromTask(async () =>
                await this.HostScreen.Router.Navigate.Execute(new EditableMenuOfUserViewModel(this.HostScreen, new User(), 1, DialogCoordinator.Instance)).Select(_ => Unit.Default));
            this.EditUserCommand = ReactiveCommand.CreateFromTask(async () =>
                await this.HostScreen.Router.Navigate
                    .Execute(new EditableMenuOfUserViewModel(this.HostScreen, this.SelectedUser, 2, DialogCoordinator.Instance))
                    .Select(_ => Unit.Default));
            this.DeleteSelectedUser = ReactiveCommand.CreateFromTask(async () =>
                await this.HostScreen.Router.Navigate
                    .Execute(new ReasonDeleteUserViewModel(this.HostScreen, this.SelectedUser, DialogCoordinator.Instance))
                    .Select(_ => Unit.Default));
            this.Users = new List<User>();
            this.userService = new UserService();
            this.MainMenu = ReactiveCommand.CreateFromTask(async () =>
            {
                await this.HostScreen.Router.NavigateAndReset.Execute(new MainMenuViewModel(this.HostScreen));
            });
            this.GetUsersList();
        }

        /// <summary>
        /// Gets return UrlSegment for Screen
        /// </summary>
        public string UrlPathSegment
        {
            get
            {
                return "Menu of show list of users and manipulation with that";
            }
        }

        /// <summary>
        /// Gets Screen
        /// </summary>
        public IScreen HostScreen { get; }

        /// <summary>
        /// Gets Edit Command
        /// </summary>
        public ReactiveCommand<Unit, Unit> EditUserCommand { get; }

        /// <summary>
        /// Gets or sets selected user
        /// </summary>
        public User SelectedUser { get; set; }

        /// <summary>
        /// Gets or sets users
        /// </summary>
        public IList<User> Users { get; set; }

        /// <summary>
        /// Gets to enter to details
        /// </summary>
        public ReactiveCommand<Unit, Unit> Back { get; }

        /// <summary>
        /// Gets command for go to MainMenu
        /// </summary>
        public ReactiveCommand<Unit, Unit> MainMenu { get; }

        /// <summary>
        /// Gets command to AddUser
        /// </summary>
        public ReactiveCommand<Unit, Unit> AddUserCommand { get; }

        /// <summary>
        /// Gets command to delete selected user
        /// </summary>
        public ReactiveCommand<Unit, Unit> DeleteSelectedUser { get; }

        /// <summary>
        /// Get users list
        /// </summary>
        public void GetUsersList()
        {
            var result = this.userService.GetUser().Where(x => x.DeletedUser == false);
            this.Users = Mapper.Map<IList<User>>(result);
        }
    }
}
