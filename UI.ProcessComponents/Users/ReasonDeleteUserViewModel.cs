// <copyright file="ReasonDeleteUserViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.ProcessComponents.Users
{
    using System;
    using System.Reactive;
    using System.Windows.Input;

    using AutoMapper;
    using MahApps.Metro.Controls.Dialogs;
    using ReactiveUI;
    using UI.Entities.Users;
    using UI.Infrastructure.ViewModel.Users;
    using VeeneroBuisness.Entities.User;
    using VeeneroBuisness.Infrastructure.Services;
    using VeeneroBuisness.Workflows.Services;

    /// <summary>
    /// ViewModel for ReasonDeleteUserView
    /// </summary>
    public class ReasonDeleteUserViewModel : ReactiveObject, IReasonDeleteUserViewModel
    {
        /// <summary>
        /// Delete User Command
        /// </summary>
        private ReactiveCommand<Unit, Unit> deleteUserCommand;

        /// <summary>
        /// User Service from server
        /// </summary>
        private IUserService userService;

        /// <summary>
        /// dialog Coordinator for controls dialogs
        /// </summary>
        private IDialogCoordinator dialogCoordinator;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReasonDeleteUserViewModel"/> class.
        /// </summary>
        /// <param name="screen">Screen of reactive</param>
        /// <param name="user">user of user</param>
        /// <param name="coordinator">coordinator for dialogs</param>
        public ReasonDeleteUserViewModel(IScreen screen, User user, IDialogCoordinator coordinator)
        {
            this.HostScreen = screen;
            this.Back = this.HostScreen.Router.NavigateBack;
            this.userService = new UserService();
            this.User = user;
            this.dialogCoordinator = coordinator;
        }

        /// <summary>
        /// Gets return Url segment
        /// </summary>
        public string UrlPathSegment
        {
            get
            {
                return "Enter reason for delete user";
            }
        }

        /// <summary>
        /// Gets screen
        /// </summary>
        public IScreen HostScreen { get; }

        /// <summary>
        /// Gets back command for navigation
        /// </summary>
        public ReactiveCommand<Unit, Unit> Back { get; }

        /// <summary>
        /// Gets command for delete user
        /// </summary>
        public ICommand DeleteUser
        {
            get
            {
                if (this.deleteUserCommand == null)
                {
                    return this.deleteUserCommand = this.DeleteUserCommand(null);
                }

                return this.deleteUserCommand;
            }
        }

        /// <summary>
        /// Gets or sets object of user
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Creating delete user command
        /// </summary>
        /// <param name="canExecute">can execute or not</param>
        /// <returns>returns command.</returns>
        public ReactiveCommand<Unit, Unit> DeleteUserCommand(IObserver<bool> canExecute)
        {
            return ReactiveCommand.Create(() =>
            {
                this.User.DeletedUser = true;
                this.userService.UpdateUser(Mapper.Map<UserDto>(this.User));
                this.HostScreen.Router.NavigateAndReset.Execute(new ControlMenuOfUsersViewModel(this.HostScreen));
            });
        }
    }
}