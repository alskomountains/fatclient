﻿// <copyright file="IViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.Infrastructure
{
    using System.Reactive;

    using ReactiveUI;

    /// <summary>
    /// Set base fore ViewModels. More implements!
    /// </summary>
    public interface IViewModel : IReactiveObject, IRoutableViewModel
    {
        /// <summary>
        /// Gets Back on ViewModels
        /// </summary>
        ReactiveCommand<Unit, Unit> Back { get; }
    }
}
