﻿// <copyright file="IMainMenuViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.Infrastructure.ViewModel.MainMenu
{
    using System.Reactive;
    using ReactiveUI;

    /// <summary>
    /// Interface for MainMenuViewModel
    /// </summary>
    public interface IMainMenuViewModel : IViewModel
    {
        /// <summary>
        /// Gets turn to ControlListOfUsers
        /// </summary>
        ReactiveCommand<Unit, Unit> RouteToControlListOfUsers { get; }
    }
}
