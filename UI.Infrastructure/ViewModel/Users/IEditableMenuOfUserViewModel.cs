﻿// <copyright file="IEditableMenuOfUserViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.Infrastructure.ViewModel.Users
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Input;
    using UI.Entities.Users;

    /// <summary>
    /// Interface for EditableMenuOfUserViewModel
    /// </summary>
    public interface IEditableMenuOfUserViewModel : IViewModel
    {
        /// <summary>
        /// Gets or sets User
        /// </summary>
        User User { get; set; }

        /// <summary>
        /// Gets command
        /// </summary>
        ICommand ApplyCommand { get; }

        /// <summary>
        /// Gets or sets state of changes in long type
        /// </summary>
        long ChangesState { get; set; }

        /// <summary>
        /// Gets or sets name of act
        /// </summary>
        string NameOfAct { get; set; }

        /// <summary>
        /// Gets or sets type of
        /// </summary>
        ObservableCollection<string> TypeOfUser { get; set; }

        /// <summary>
        /// Gets or sets list of user types
        /// </summary>
        IList<UserType> UserTypes { get; set; }
    }
}
