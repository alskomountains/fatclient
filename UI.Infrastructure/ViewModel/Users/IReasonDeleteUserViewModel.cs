// <copyright file="IReasonDeleteUserViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.Infrastructure.ViewModel.Users
{
    using System.Reactive;
    using System.Windows.Input;
    
    using ReactiveUI;
    using UI.Entities.Users;
    
    /// <summary>
    /// Interface for ReasonDeleteUserViewModel
    /// </summary>
    public interface IReasonDeleteUserViewModel : IViewModel
    {
        /// <summary>
        /// Gets command for delete user
        /// </summary>
        ICommand DeleteUser { get; }
        
        /// <summary>
        /// Gets or sets user
        /// </summary>
        User User { get; set; }
    }
}