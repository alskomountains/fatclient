﻿// <copyright file="IControlMenuOfUsersViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.Infrastructure.ViewModel.Users
{
    using System.Collections.Generic;
    using System.Reactive;
    using ReactiveUI;
    using UI.Entities.Users;

    /// <summary>
    /// Interface for ControlMenuOfUsersViewModel
    /// </summary>
    public interface IControlMenuOfUsersViewModel : IViewModel
    {
        /// <summary>
        /// Gets Add new user
        /// </summary>
        ReactiveCommand<Unit, Unit> AddUserCommand { get; }

        /// <summary>
        /// Gets edit exists user
        /// </summary>
        ReactiveCommand<Unit, Unit> EditUserCommand { get; }

        /// <summary>
        /// Gets delete selected user
        /// </summary>
        ReactiveCommand<Unit, Unit> DeleteSelectedUser { get; }

        /// <summary>
        /// Gets or sets selected user
        /// </summary>
        User SelectedUser { get; set; }

        /// <summary>
        /// Gets command for go to MainMenu
        /// </summary>
        ReactiveCommand<Unit, Unit> MainMenu { get; }

        /// <summary>
        /// Gets or sets users
        /// </summary>
        IList<User> Users { get; set; }
    }
}
