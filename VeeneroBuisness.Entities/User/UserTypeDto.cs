﻿namespace VeeneroBuisness.Entities.User
{
    using System;

    using VeeneroBuisness.Entities;
    using VeeneroBuisness.Entities.User;

    /// <summary>
    /// Class of UserType on Server
    /// </summary>
    public class UserTypeDto : IHasUIdDto
    {
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets Type
        /// </summary>
        public string Type { get; set; }
    }
}
