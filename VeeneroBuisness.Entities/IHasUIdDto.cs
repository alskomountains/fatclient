﻿namespace VeeneroBuisness.Entities
{
    using System;

   public interface IHasUIdDto
   {
       Guid Id { get; set; }
   }
}
