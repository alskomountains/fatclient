﻿// <copyright file="MainMenuView.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.VisualComponents.MainMenu
{
    using System.Windows;
    using System.Windows.Input;
    using ReactiveUI;
    using UI.Infrastructure.ViewModel.MainMenu;

    /// <summary>
    /// Interaction logic for MainMenuView
    /// </summary>
    public partial class MainMenuView : IViewFor<IMainMenuViewModel>
    {
        /// <summary>
        /// Register ViewModel in code-behavior
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(IMainMenuViewModel), typeof(MainMenuView), new PropertyMetadata(null));

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuView"/> class.
        /// </summary>
        public MainMenuView()
        {
            this.InitializeComponent();

            this.WhenActivated(d =>
            {
                d(this.WhenAnyValue(x => x.ViewModel).BindTo(this, x => x.DataContext));
                d(this.BindCommand(ViewModel, vm => vm.RouteToControlListOfUsers, view => view.RouteToUsers));
            });
        }

        /// <summary>
        /// Gets or sets getValue
        /// </summary>
        public IMainMenuViewModel ViewModel
        {
            get
            {
                return (IMainMenuViewModel)this.GetValue(ViewModelProperty);
            }

            set
            {
                this.SetValue(ViewModelProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets ViewModel
        /// </summary>
        object IViewFor.ViewModel
        {
            get { return this.ViewModel; }
            set { this.ViewModel = (IMainMenuViewModel)value; }
        }

        /// <summary>
        /// Sets open popup
        /// </summary>
        /// <param name="sender">Sender for send</param>
        /// <param name="e">E for event</param>
        private void UIElement_OnMouseEnter(object sender, MouseEventArgs e)
        {
            PopupErrorGroups.IsOpen = true;
        }
    }
}
