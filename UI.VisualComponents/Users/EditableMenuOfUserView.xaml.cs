﻿// <copyright file="EditableMenuOfUserView.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.VisualComponents.Users
{
    using System.Windows;
    using ReactiveUI;
    using UI.Infrastructure.ViewModel.Users;

    /// <summary>
    /// Interaction logic for EditableMenuOfUserView
    /// </summary>
    public partial class EditableMenuOfUserView : IViewFor<IEditableMenuOfUserViewModel>
    {
        /// <summary>
        /// Register View in code-behind for ViewModel
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(
            "ViewModel", typeof(IEditableMenuOfUserViewModel), typeof(EditableMenuOfUserView), new PropertyMetadata(default(IEditableMenuOfUserViewModel)));

        /// <summary>
        /// Initializes a new instance of the <see cref="EditableMenuOfUserView"/> class.
        /// </summary>
        public EditableMenuOfUserView()
        {
            this.InitializeComponent();
            this.WhenActivated(d =>
            {
                d(this.BindCommand(this.ViewModel, vm => vm.Back, view => view.Cancel));
                d(this.Bind(this.ViewModel, vm => vm.User.Name, view => view.NameOfUser.Text));
                d(this.Bind(this.ViewModel, vm => vm.User.Description, view => view.DescriptionOfUser.Text));
                d(this.Bind(this.ViewModel, vm => vm.NameOfAct, view => view.GroupBoxMain.Header));
                d(this.BindCommand(this.ViewModel, vm => vm.ApplyCommand, view => view.Apply));
                this.DataContext = this.ViewModel;
            });
        }

        /// <summary>
        /// Gets or sets Value
        /// </summary>
        public IEditableMenuOfUserViewModel ViewModel
        {
            get
            {
                return (IEditableMenuOfUserViewModel)this.GetValue(ViewModelProperty);
            }

            set
            {
                this.SetValue(ViewModelProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets ViewModel
        /// </summary>
        object IViewFor.ViewModel
        {
            get
            {
                return this.ViewModel;
            }

            set
            {
                this.ViewModel = (IEditableMenuOfUserViewModel)value;
            }
        }
    }
}
