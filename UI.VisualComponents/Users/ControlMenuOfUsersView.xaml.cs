﻿// <copyright file="ControlMenuOfUsersView.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.VisualComponents.Users
{
    using System.ComponentModel.Composition;
    using System.Windows;

    using MahApps.Metro.Controls;
    using MahApps.Metro.Converters;
    using ReactiveUI;
    using UI.Infrastructure.ViewModel.Users;
    using UI.ProcessComponents.Users;

    /// <summary>
    /// Interaction logic for ControlMenuOfUsersView
    /// </summary>
    public partial class ControlMenuOfUsersView : IViewFor<IControlMenuOfUsersViewModel>
    {
        /// <summary>
        /// Register ViewModelProperty in code-behavior
        /// </summary>
        public static readonly DependencyProperty PropertyTypeProperty = DependencyProperty.Register(
            "ViewModel", typeof(IControlMenuOfUsersViewModel), typeof(ControlMenuOfUsersView), new PropertyMetadata(default(ControlMenuOfUsersView)));

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlMenuOfUsersView"/> class.
        /// Constructor for Initialize Control
        /// </summary>
        public ControlMenuOfUsersView()
        {
            this.InitializeComponent();
            this.WhenActivated(d =>
            {
                d(this.BindCommand(this.ViewModel, vm => vm.Back, view => view.Back));

                d(this.BindCommand(this.ViewModel, vm => vm.AddUserCommand, view => view.AddNewUser));

                d(this.OneWayBind(this.ViewModel, vm => vm.Users, view => view.UsersDataGrid.ItemsSource));

                d(this.BindCommand(this.ViewModel, vm => vm.MainMenu, view => view.MainMenu));

                d(this.Bind(this.ViewModel, vm => vm.SelectedUser, view => view.UsersDataGrid.SelectedItem));
            });
        }

        /// <summary>
        /// Gets or sets Value
        /// </summary>
        public IControlMenuOfUsersViewModel ViewModel
        {
            get { return (ControlMenuOfUsersViewModel)this.GetValue(PropertyTypeProperty); }
            set { this.SetValue(PropertyTypeProperty, value); }
        }

        /// <summary>
        /// Gets or sets ViewModel
        /// </summary>
        object IViewFor.ViewModel
        {
            get { return this.ViewModel; }
            set { this.ViewModel = (IControlMenuOfUsersViewModel)value; }
        }
    }
}
