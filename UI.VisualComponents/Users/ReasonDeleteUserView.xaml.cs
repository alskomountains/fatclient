﻿// <copyright file="ReasonDeleteUserView.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.VisualComponents.Users
{
    using System.Windows;

    using MahApps.Metro.Controls;
    using ReactiveUI;
    using UI.Infrastructure.ViewModel.Users;

    /// <summary>
    /// Logic for ReasonDeleteUserView
    /// </summary>
    public partial class ReasonDeleteUserView : IViewFor<IReasonDeleteUserViewModel>
    {
        /// <summary>
        /// Register dependency property of viewModel
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(
            "ViewModel", typeof(IReasonDeleteUserViewModel), typeof(ReasonDeleteUserView), new PropertyMetadata(default(IReasonDeleteUserViewModel)));

        /// <summary>
        /// Initializes a new instance of the <see cref="ReasonDeleteUserView"/> class.
        /// </summary>
        public ReasonDeleteUserView()
        {
            this.InitializeComponent();
            this.ComboxBoxOfView.Header = "Delete user";
            this.WhenActivated(d =>
            {
                d(this.BindCommand(this.ViewModel, vm => vm.Back, view => view.Cancel));
                d(this.BindCommand(this.ViewModel, vm => vm.DeleteUser, view => view.Delete));
                d(this.Bind(this.ViewModel, vm => vm.User.ReasonOfDeleted, view => view.TextBoxOfReason.Text));
            });
        }

        /// <summary>
        /// Gets or sets value of viewModel
        /// </summary>
        public IReasonDeleteUserViewModel ViewModel
        {
            get { return (IReasonDeleteUserViewModel)this.GetValue(ViewModelProperty); }
            set { this.SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        /// Gets or sets object viewModel
        /// </summary>
        object IViewFor.ViewModel
        {
            get { return this.ViewModel; }
            set { this.ViewModel = (IReasonDeleteUserViewModel)value; }
        }
    }
}
