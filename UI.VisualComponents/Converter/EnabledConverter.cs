﻿// <copyright file="EnabledConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace UI.VisualComponents.Converter
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Converter for Enabling or disabling something
    /// </summary>
    public class EnabledConverter : IValueConverter
    {
        /// <summary>
        /// Convert forward
        /// </summary>
        /// <param name="value">set value</param>
        /// <param name="targetType">set target type</param>
        /// <param name="parameter">sets parameter</param>
        /// <param name="culture">sets culture</param>
        /// <returns>Returns parameter</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                return "true";
            }

            return "false";
        }

        /// <summary>
        /// Convert back
        /// </summary>
        /// <param name="value">set value</param>
        /// <param name="targetType">set target type</param>
        /// <param name="parameter">sets parameter</param>
        /// <param name="culture">sets culture</param>
        /// <returns>Returns parameter</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? parameter : null;
        }
    }
}
