using System;
using DataAccessLayer.Components.Repositories.User;

namespace DataAccessLayer.Components.Repositories
{
    using DataAccessLayer.Infrastructure;
    using VeeneroLibraryModel.User;
    
    public class UnitOfWork : IUnitOfWork
    {
        private UserRepository _userRepository;

        private UserTypeRepository _userTypeRepository;

        private bool _disposed = false;
        
        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                disposing = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IRepository<VeeneroLibraryModel.User.User> UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository();
                }
                return _userRepository;
            }
            
        }

        public IRepository<UserType> UserTypeRepository
        {
            get
            {
                if (_userTypeRepository == null)
                {
                    _userTypeRepository = new UserTypeRepository();
                }

                return _userTypeRepository;
            }
        }
    }
}