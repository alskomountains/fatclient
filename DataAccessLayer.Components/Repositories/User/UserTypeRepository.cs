﻿using System.Linq;

namespace DataAccessLayer.Components.Repositories.User
{
    using System.Collections.Generic;

    using DataAccessLayer.Infrastructure;
    using VeeneroLibraryModel.User;
    using LiteDB;


    public class UserTypeRepository : IRepository<UserType>
    {
        private readonly LiteDatabase liteDatabase;

        public UserTypeRepository()
        {
            liteDatabase = new LiteDatabase(@"Base.db");
        }

        public IList<UserType> GetList()
        {
            return LiteCollection().FindAll().ToList();
        }

        public UserType GetById(int id)
        {
            throw new System.NotImplementedException();
        }

        public void Create(UserType item)
        {
            throw new System.NotImplementedException();
        }

        public void Update(UserType item)
        {
            throw new System.NotImplementedException();
        }

        private LiteCollection<UserType> LiteCollection()
        {
            var collection = liteDatabase.GetCollection<UserType>("UsersType");

            if (collection.Find(Query.All(Query.Descending)).ToList().Count <= 0)
            {
                var types = new List<UserType>
                {
                    new UserType { Type = "Enemy" },
                    new UserType { Type = "Friendly" },
                    new UserType { Type = "Neutral" }
                };
                collection.Upsert(types);
            }

            return collection;
        }
    }
}
