﻿using System.Linq;

namespace DataAccessLayer.Components.Repositories.User
{
    using System.Collections.Generic;

    using DataAccessLayer.Infrastructure;
    using VeeneroLibraryModel.User;
    using LiteDB;

    public class UserRepository : IRepository<User>
    {
        private readonly LiteDatabase liteDatabase;

        public UserRepository()
        {
            liteDatabase = new LiteDatabase(@"Base.db");     
        }

        public IList<User> GetList()
        {
            return LiteCollection().FindAll().ToList();
        }

        public User GetById(int id)
        {
            return LiteCollection().FindById(id);
        }

        public void Create(User item)
        {
            LiteCollection().Insert(item);
        }

        public void Update(User item)
        {
            LiteCollection().Update(item);
        }

        private LiteCollection<User> LiteCollection()
        {
            var collection = liteDatabase.GetCollection<User>("Users");

            return collection;
        }
    }
}
